import requests

def test_home_page_content():
    response = requests.get('http://192.168.1.25:5000')
    assert response.status_code == 200
    assert 'Ceci est une application Flask de démonstration' in response.text

def test_about_page_content():
    response = requests.get('http://192.168.1.25:5000/about')
    assert response.status_code == 200
    assert 'Cette application Flask est utilisée pour présenter les fonctionnalités de base de Flask' in response.text
