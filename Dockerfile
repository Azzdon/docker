# Utilisez une image Python slim-buster comme image de base
FROM python:3.9-slim-buster

# Définissez le répertoire de travail dans le conteneur
WORKDIR /app

# Copiez les fichiers de dépendances dans le conteneur
COPY requirements.txt .

# Installez les dépendances
RUN pip install --no-cache-dir -r requirements.txt

# Copiez les fichiers de l'application dans le conteneur
COPY app.py .
COPY templates ./templates
COPY boot.sh .

# Définissez la variable d'environnement APP_ENVIRONMENT
# Sa valeur par défaut est 'PROD', mais elle peut être écrasée à l'exécution du conteneur
ENV APP_ENVIRONMENT=PROD

# Exposez le port 5000
EXPOSE 5000

# Exécutez le script de démarrage à l'exécution du conteneur
CMD ["./boot.sh"]

