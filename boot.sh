#!/bin/bash

set -e

if [ "$APP_ENVIRONMENT" = 'DEV' ]; then
    echo "Running Development Server"
    export FLASK_APP=app.py
    FLASK_ENV=development exec flask run -h 0.0.0.0
else
    echo "Running Production Server"
    exec gunicorn -b :5000 --access-logfile - --error-logfile - app:app
fi

